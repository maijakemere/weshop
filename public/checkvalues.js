window.onload = function() {
    var button=document.getElementById("submit");
    button.onclick = check_results;
}

function validate_data() {
    var name;
    var manufacture;
    var info;
    var price;
    var picture;
                    
    var message="";
    var not_valid=0;
                    
    name=document.getElementById("name");
    manufacture=document.getElementById("manufacture");
    info=document.getElementById("info");
    price=document.getElementById("price");
    picture=document.getElementById("picture");
                
    if(name.value==""||name.value.length<3) {
        name.style.backgroundColor="red";
        not_valid++;
        message="Ievadi nosaukumu (tam ir jābūt vairāk kā 3 simbolus garam)!";
    }
    else name.style.backgroundColor="white";
                
    if(manufacture.value=="") {
        manufacture.style.backgroundColor="red";
        not_valid++;
        if (message!=="") {
            message+=" Ievadi ražotāju!";
        }
        else message="Ievadi ražotāju!";
    }
    else manufacture.style.backgroundColor="white";
                
    if(info.value=="") {
        info.style.backgroundColor="red";
        not_valid++;
        if (message!=null){
            message+=" Ievadi informāciju par preci!";
        }
        else message="Ievadi informāciju par preci!";
    }
    else info.style.backgroundColor="white";
                
    var regular=/[0-9]+\.[0-9][0-9]/;
    var tester=price.value;
    var result=regular.test(tester);
                
    if(tester==""||result==false) {
        price.style.backgroundColor="red";
        not_valid++;
        if (message!=null){
            message+=" Ievadi preces cenu (tā drīkst saturēt tikai ciparus un 2 ciparus aiz decimālzīmes!";
        }
        else message="Ievadi preces cenu (tā drīkst saturēt tikai ciparus un 2 ciparus aiz decimālzīmes!";
    }
    else price.style.backgroundColor="white";
                
    var image=picture.value;
    var ex=image.split('.').pop();
    var extension=String(ex);
                
    if(extension!="jpg"&&extension!="png"&&extension!="jpeg"&&extension!="gif"&&extension!="JPG"&&extension!="PNG"&&extension!="JPEG"&&extension!="GIF"&&extension!="") {
        picture.style.backgroundColor="red";
        not_valid++;
        if (message!=null){
            message+=" Preces attēls var būt tikai .png, .jpg, .jpeg un .gif formātos";
        }
    }
                
    return [not_valid, message];
}
                        
function check_results() {
    var check=validate_data();
    var num=parseInt(check[0]);
    var message=check[1];
    if (num>0) {
        alert(message);
        return false;
    }
    return true;
}