<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Labās lapas BE</title>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
        <link rel="stylesheet" type="text/css" href="/weshop/public/style.css">
        <script type="text/javascript" src="/weshop/public/checkvalues.js" ></script>
    </head>
    <body>
        <div id="container">
        <a href="/weshop/admin/create" class="bemenu">Pievienot produktu</a>
        <a href="/weshop/admin/" class="bemenu">BE lapa</a>
        <a href="/weshop/index" class="bemenu">FE lapa</a>
        <a href="/weshop/admin/purchases" class="bemenu">Pirkumi</a>
        <a href="/weshop/admin/categories" class="bemenu">Kategorijas</a>
        <a href="/weshop/admin/customs" class="bemenu">Propertiji</a>
        <?php 
            include_once("application/controllers/Admin/Admin.php");
            $controller = new Admin();
            
            if(!isset($_SERVER['PATH_INFO'])) {
                $controller->showProducts(0);
            }
            
            else {
                $param=$_SERVER['PATH_INFO'];
                $pieces = explode('/', $param);
                
                $i=1;
                //var_dump($pieces);
                
                if($pieces[$i]=="id"){
                    $controller->showProduct($pieces[$i+1]);
                }
                
                else if($pieces[$i]=="edit"){
                    $controller->editProduct($pieces[$i+1]);
                }
                
                else if($pieces[$i]=="create") {
                    $controller->createProduct();
                }
                
                else if($pieces[$i]=="add") {
                    $controller->addProduct();
                }
                
                else if($pieces[$i]=="delete") {
                    $controller->deleteProduct($pieces[$i+1]);
                }
            
                else if($pieces[$i]=="purchases") {
                    $controller->viewPurchases();
                }
            
                else if($pieces[$i]=="purchase") {
                    $controller->viewPurchasDetails($pieces[$i+1]);
                }
                
                else if($pieces[$i]=="categories") {
                    $controller=viewCategories();
                }
                
                /*else if($pieces[$i]=="customs") {
                    $controller=viewCustoms();
                }*/
                
                else {
                    $controller->showProducts();
                }
            }
            
        ?>
        </div>
    </body>
</html>