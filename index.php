<?php
    error_reporting(E_ALL);
    date_default_timezone_set('Europe/Riga');
    include_once 'header.php';
    /*function __autoload($class_name)
    {
        require_once "application/controllers/".$class_name.".php";
    }*/
    function autoload($className)
    {
    //list comma separated directory name
        $directory = array('application/controllers/', 'application/controllers/Admin/',
                            'application/models/', 'application/models/Admin/',
                            'application/views/', 'application/views/Admin/');

    //list of comma separated file format
        $fileFormat = array('%s.php', '%s.class.php');

        foreach ($directory as $current_dir)
        {
            foreach ($fileFormat as $current_format)
            {

                $path = $current_dir.sprintf($current_format, $className);
                if (file_exists($path))
                {
                    include $path;
                    return ;
                }
            }
        }
    }
    spl_autoload_register('autoload');
    $db=new Database();
    $db->connect();
    $controller = new Core();
    $contpath = new Controller();
    $contpath->categories();
    $controller->run();

    include_once 'footer.php';