<div id="cart">
    <?php
        $items = array();

        /** @var $arr array */
        $items = $arr[0];

        if (empty($items)) {
            echo '<div class="noproduct">Tavā grozā vēl nav produktu</div>';
        } else {
            echo '<div class="tables"><table>';
            echo "<tr>";
            echo '<td><h3>Produkta attēls</h3></td>';
            echo "<td><h3>Produkta nosaukums</h3></td>";
            echo "<td><h3>Informācija</h3></td>";
            echo "<td><h3>Cena</h3></td>";
            echo "<td><h3>Skaits</h3></td>";
            echo "<td></td>";
            echo "</tr>";

            foreach ($items as $item) {
                echo "<tr>";
                echo '<td><img src="/weshop/public/images/' . $item->picture . '" class="cartimage"/></td>';
                echo "<td><h3>" . $item->name . "</h3></td>";
                echo "<td>" . $item->info . "</td>";
                echo "<td><h3>" . $item->price . "</h3></td>";
                echo "<td><h3>" . $item->quantity . "</h3></td>";
                echo "<td>";
                if (!empty($item->arr)) {
                    echo "<a href='/weshop/index/config/" . $item->id . "' class='remove'>Change configuration</a>";
                }
                echo "<a href='/weshop/index/remove/" . $item->id . "' class='remove'>Remove item</a></td>";
                echo "</tr>";
            }
            echo "</table></div>";

            echo '<form action="/weshop/index/checkout" name="checkout" method="post" id="checkout">
                <input type="submit" id="submit" value="Iegādāties"></form>';
        }
    ?>
</div>