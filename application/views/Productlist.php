<div id="products">
<?php
    $products = array();
    $pages = array();
    $category = '';

    /** @var $arr array */
    $products=$arr[0];
    $pages=$arr[1];

    if(empty($products)) {
        echo '<div class="noproduct">Šādu produktu nav.</div>';
    }
    if (isset($products)) {
        foreach ($products as $product)
        {
            echo '<div class="product"><div class="prodim">
            <img src="/weshop/public/images/thumbs/'.$product->picture.'" class="frontimage"/> </div>
            <div class="prodinfo"><h3><a href="/weshop/index/id/'.$product->id.'">'.$product->name.'</a></h3>
            <span>'.$product->info.'</span><span class="price">Ls '.$product->price.'</span>
            <form action="/weshop/index/id/'.$product->id.'" method="post">
                <input type="hidden" value="'.$product->id.'" name="id"/>
                <input type="submit" value="Skatīt"  />
            </form>
            </div></div>';
        }
    }
    if (isset($pages)) {
        if($pages > 1) {
            echo '<div class="paginator">';
            $param="";
            $pieces = array();
            if (isset($_SERVER['PATH_INFO'])) {
                $param = $_SERVER['PATH_INFO'];
                $pieces = explode('/', $param);
            }
            $bool = false;
            for($i = 2; $i < sizeof($pieces); $i += 2 )
            {
                if($pieces[$i] == "category") {
                    $category = $pieces[$i+1];
                    $bool = true;
                }
            }
            for ($i=1; $i<$pages+1; $i++)
            {
                $a=$i-1;
                //Te vajadzētu smuku funkciju, kas masīvā ielasa visas url padotos mainīgos un uzkonstruē url no jauna. Maybe tomorrow!
                if(!empty($pieces)) {
                    if($bool==true) {
                        echo '<a href="/weshop/index/category/'.$category.'/p/'.$a.'">'.$i.'</a>';
                    } else {
                        echo '<a href="/weshop/index/p/'.$a.'">'.$i.'</a>';
                    }
                }  else {
                    echo '<a href="/weshop/index/p/'.$a.'">'.$i.'</a>';
                }
            }
            echo '</div>';
        }
    }
?>
</div>           

