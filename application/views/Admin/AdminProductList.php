<?php 
    if(empty($products)) {
        echo "Šādu produktu nav.";
    }
    
    else {
        echo '<table border="1">';
        foreach ($products as $product) {
            echo '<tr><td><img src="/weshop/public/images/thumbs/'.$product->picture.'" class="small"/></td>';
            echo '<td>'.$product->name.'</td>';
            echo '<td>'.$product->info.'</td>';
            echo '<td>'.$product->price.'</td>';
            echo '<td><a href="/weshop/admin/id/'.$product->id.'" class="remove">Labot</a></td>';
            echo '<td><a href="/weshop/admin/delete/'.$product->id.'" class="remove">Dzēst</a></td></tr>';
        }
        echo '</table>';
    }