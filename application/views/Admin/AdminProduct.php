<?php
    $bool=false;
    if(empty($product)) {
        echo "Šādu produktu nav.";
    }

    else {
        $prodtype="";
        $prodcat="";
        foreach($product as $prod) {
            $po=new Product($prod->id, $prod->type, $prod->category, $prod->name, $prod->manufacture, $prod->info,
                            $prod->price, $prod->picture);
        }
        if (isset($types)) {
            foreach($types as $type) {
                if($type->id==$po->type) {
                    $prodtype=$type->name;
                    break;
                }
                else {
                $prodtype="Šāds tips vairs neeksistē";
                }
            }
        }

        if (isset($cats)) {
            foreach($cats as $cat) {
                if($cat->id==$po->category) {
                    $prodcat=$cat->name;
                    break;
                }
                else {
                    $procat="Šis kaķītis ir pazudis";
                }
            }
        }

        echo '<table border="1"><tr><td>Attēls</td><td>Tips</td><td>Kategorija</td><td>Nosaukums</td><td>Ražotājs</td>
            <td>Informācija</td><td>Cena</td><td>Labot</td><td>Dzēst</td></tr>
            <tr><td><img src="/weshop/public/images/thumbs/'.$po->picture.'" class="small"/></td><td>'.$prodtype.'</td>
            <td>'.$prodcat.'</td><td>'.$po->name.'</td>
            <td>'.$po->manufacture.'</td><td>'.$po->info.'</td><td>'.$po->price.'</td>
            <td><a href="/weshop/admin/id/'.$po->id.'" class="remove">Labot</a></td>
            <td><a href="/weshop/admin/delete/'.$po->id.'" class="remove">Dzēst</a></td></tr></table>';
    }
?>
        <div id="addnew">
            <form action="/weshop/admin/edit/<?php echo $po->id; ?>" method="post" name="myform" id="myform"
                  enctype="multipart/form-data">
                <?php
                    if(isset($customs)&&!isset($allcustoms)) {
                        foreach($customs as $list) {
                            echo '<div><div>'.$list[0]->name.':</div>';
                            foreach ($list as $config) {
                                echo '<input type="checkbox" name="'.$config->name.'" value="'.$config->id.'">
                                    '.$config->value;
                            }
                            echo '</div></br>';
                        }
                    }

                    else if(isset($allcustoms)&&isset($customs)) {
                        foreach($customs as $list) {
                            echo '<div><div>'.$list[0]->name.':</div>';
                            foreach ($list as $config) {
                                foreach($allcustoms as $checked) {
                                    if ($checked->customId==$config->id) {
                                        $bool=true;
                                        break;
                                    }
                                }
                                if($bool==true){
                                    echo '<input type="checkbox" name="property[]" value="'.$config->id.'" checked>'.$config->value;
                                }
                                else {
                                    echo '<input type="checkbox" name="property[]" value="'.$config->id.'">'.$config->value;
                                }
                                $bool=false;
                            }
                            echo '</div></br>';
                        }
                    }
                ?>
                <div><label for="type">Tips </label><select name="type" id="type">
                    <?php
                        if (isset($types)) {
                            foreach($types as $type) {
                                echo '<option value="'.$type->id.'">'.$type->name.'</option>';
                            }
                        }
                    ?>
                </select>
                </div>
                <div><label for="category">Kategorija</label> <select name="category" id="category">
                    <?php
                        if (isset($categories)) {
                            foreach($categories as $category) {
                                echo $category->string;
                            }
                        }
                    ?>
                </select>
                </div>
                <div><label for="name">Nosaukums </label><input name="name" type="text" id="name" value="<?php echo $po->name; ?>"></div>
                <div><label for="manufacture">Ražotājs </label><input name="manufacture" type="text" id="manufacture" value="<?php echo $po->manufacture; ?>"></div>
                <div><label for="info">Info </label><textarea name="info" id="info"><?php echo $po->info; ?></textarea></div>
                <div><label for="price">Cena </label><input name="price" type="text" id="price" value="<?php echo $po->price; ?>"></div>
                <div><label for="picture">Attēls </label><input name="picture" type="file" id="picture"></div>
                <div><input type="submit" id="submit" value="Labot"></div>
            </form>
        </div>