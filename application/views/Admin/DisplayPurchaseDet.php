<?php
    if(!empty($purchases)) {
        echo '<table border="1"><tr><td>Produkta ID</td><td>Produkta nosaukums</td><td>Skaits</td><td>Konfigurācija</td></tr>';
        foreach($purchases as $purchase) {
            echo '<tr><td>'.$purchase->id.'</td><td>'.$purchase->name.'</td><td>'.$purchase->count.'</td><td>'.$purchase->config.'</td></tr>';
        }
        echo '</table>';
    }
    else {
        echo "Ups! Esam pazaudējuši pirkuma datus!";
    }