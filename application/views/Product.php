<?php
    $product = array();
    $customs = array();


    /** @var $arr array */
    $product = $arr[0];
    $customs = $arr[1];

    foreach($product as $prod) {
            $po=new Product($prod->id, $prod->type, $prod->category, $prod->name, $prod->manufacture, $prod->info,
                            $prod->price, $prod->picture);
        }

    echo '<div id="singleprod"><div class="sprodim"><img src="/weshop/public/images/'.$po->picture.'" class="sprodimage" /></div>';
    echo '<div class="sprodinfo"><h3>'.$po->name.'</h3>';
    echo '<span>'.$po->info.'</span>';
    echo '<span class="price">Ls '.$po->price.'</span>';
    if(isset($customs)) {
        echo '<form action="/weshop/index/item/'.$po->id.'" method="post">';
        foreach($customs as $list) {
            echo '<select class="config" name="'.$list[0]->name.'">';
            echo '<option disabled="disabled" selected="selected">'.$list[0]->name.'</option>';
            foreach ($list as $config) {
                echo '<option value="'.$config->id.'">'.$config->value.'</option>';
            }
            echo '</select>';
        }
        echo '<input type="submit" value="Add to Cart"  />
            </form></div></div>';
    }
    else {
        echo '<form action="/weshop/index/item/'.$po->id.'" method="post">
            <input type="submit" value="Add to Cart"  />
            </form></div></div>';
    }
           
