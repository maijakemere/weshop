<?php

    /*include_once ('Database.php');
    include_once ('Product.php');
    include_once ('Category.php');
    include_once ('Cart.php');
    include_once ('Book.php');
    include_once ('Configurables.php');
    include_once ('MySql.php');*/
    
    class Model
    {
    
        public function getOutput($query)
        {
            $db=new MySql($query);
            $res=$db->query($query);
            $result=$db->getResult();
            $output=$db->expandResults($result);
            return $output;
        }
        
        public function getPages()
        {
            $query="SELECT * FROM product";
            $db=new MySql($query);
            $res=$db->query($query);
            $result=$db->getResult();
            $num_rows=mysql_num_rows($result);
            $items=6;
            $page_ammount=ceil($num_rows/$items);
            return $page_ammount;
        }
        
        public function getCatPages($id)
        {
            $query="SELECT product.id FROM product INNER JOIN category ON product.category=category.id 
                    WHERE category.id=$id OR category.parent=$id";
            $db=new MySql($query);
            $res=$db->query($query);
            $result=$db->getResult();
            $num_rows=mysql_num_rows($result);
            $items=6;
            $page_ammount=ceil($num_rows/$items);
            return $page_ammount;
        }
               
        public function getProductList($p)
        {
            /*if(isset($_GET['p'])) {
                $page=mysql_real_escape_string($_GET['p']);
            }
            else $page=0;*/ 
            $page=$p;
            if($page<1) {
                $page = 0;
            }
            $items=6;
            $p_num=$items*$page;
            $query="SELECT * FROM product order by id DESC LIMIT $p_num, $items";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function getProduct($id)
        {
            $query="SELECT * FROM product where id=$id";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function getCatProducts($id)
        {
            $page = '';
            if(isset($_SERVER['PATH_INFO'])) {
                $param=$_SERVER['PATH_INFO'];
                $pieces = explode('/', $param);
            }
            
            if (isset($pieces[4])) {
                if($pieces[4]=="p") {
                    $page=$pieces[5];
                }
            }
            else $page=0;
            if($page<"1") {
                $page = "0";
            }
            $items=6;
            $pages=$this->getCatPages($id);
            $p_num=$items*$page;
            $query="SELECT product.id, product.type, product.category, product.name, product.manufacture, 
                    product.info, product.price, product.picture FROM product INNER JOIN category ON product.category=category.id 
                    WHERE category.id=$id OR category.parent=$id order by product.id DESC LIMIT $p_num, $items";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function showCategories()
        {
            $query = "SELECT * FROM category ORDER BY parent, name";
            $result = mysql_query($query);
            $resarr=array();
            $arr=array(
                'items'=>array(),
                'parents'=>array()
            );
            while ($row = mysql_fetch_assoc($result)) { 
                $arr['items'][$row['id']] = $row; 
                $arr['parents'][$row['parent']][] = $row['id']; 
            } 
            $format = $this->buildMenu(0, $arr);
            /*
            * Pamatīgs caur žirafes kaklu variants, jāizdomā kaut kas jaukāks ^_^
            */
            $output=new Category($format);
            $resarr[]=$output;
            return $resarr;
        }
        
        function buildMenu($id, $arr)
        {
            $html = ''; 
            if (isset($arr['parents'][$id])) { 
                $html = '<ul>'; 
                foreach ($arr['parents'][$id] as $itemId) 
                { 
                    $html .= '<li><a href="/weshop/index/category/'.$arr['items'][$itemId]['id'].'">' . $arr['items'][$itemId]['name']; 
                    $html .= $this->buildMenu($itemId, $arr); 
                    $html .= '</a></li>'; 
                } 
                $html .= '</ul>'; 
            } 
            return $html; 
        } 
        
        function createCart($id)
        {
            $cart='';
            if(isset($_SESSION['cart'])) {
                $cart = unserialize($_SESSION['cart']);
            }
            if(isset($cart)) {
            if(sizeof($cart) == 0) { 
                $cart = array(); 
            }
            }
            
            $arr=array();
            
            if(!empty($_POST)) {
                foreach($_POST as $post) {
                    $arr[]=$post;
                }
            }
            
            $query = "SELECT id, name, info, price, picture FROM product WHERE id = '$id'";
            $result = mysql_query($query);
            while($row = mysql_fetch_object($result)) {
                $quantity = 1;
                $item_data=new Cart($row->id, $row->name, $row->info, $row->price, $row->picture, $quantity, $arr);
            }
            
            $cart = $this->addToCart($cart, $item_data);
            $_SESSION['cart'] = serialize($cart);
            return $cart;
        }
        
        function addToCart($cart, $item_data)
        {
            $in_cart = FALSE;
            if(is_array($cart)) {
                foreach ($cart as $cartItem) {
                    if($cartItem->id == $item_data->id) {
                        $prev_quantity = intval($cartItem->quantity);
                        $new_quantity = $prev_quantity + intval($item_data->quantity);
                        $cartItem->quantity = $new_quantity;
                        $in_cart = TRUE;
                        break;
                    }
                }
            }
            if($in_cart == FALSE) {
                $cart[]= $item_data;
            }
            return $cart;
        }
        
        function removeFromCart($id)
        {
            $cart = unserialize($_SESSION['cart']);
            $i=-1;
            $val=$id;
            $keys=array_keys($cart);
            /*foreach($keys as $value) {
                echo $value;
            }*/
            foreach ($cart as $cartItem) {
                $i++;
                if ($cartItem->id==$id) {
                    break;
                }
            }
            if ($i>-1) {
                unset($cart[$keys[$i]]);
            }
            $_SESSION['cart'] = serialize($cart);
            return $cart;
        }
        
        /*function removeProdCart($id) {
            $cart = unserialize($_SESSION['cart']);
            $i=-1;
            $keys=array_keys($cart);
            if(!empty($keys)) {
                foreach ($cart as $cartItem) {
                    $i++;
                    if ($cartItem->id==$id) {
                        break;
                    }
                }
                echo $i;
                unset($cart[$keys[$i]]);
            }
            $_SESSION['cart'] = serialize($cart);
            return $cart;
        }*/
        
        function returnCart()
        {
            if(isset($_SESSION['cart'])) {
            $cart = unserialize($_SESSION['cart']);
            return $cart;
            }
        }
        
        public function getCustom($id) {
            $arr=array();
            $query="SELECT customitems.id, customitems.name, customitems.value
                    FROM custom INNER JOIN customitems ON custom.customId=customitems.id WHERE custom.prodId=$id ORDER BY customitems.name";
            /*$result = mysql_query($query);
            while($row=mysql_fetch_object($result)) {
                $item=new Configurables($row->id, $row->name,$row->value);
                $arr[]=$item;
            }*/
            $output=$this->getOutput($query);
            //return $output;
            $return=$this->splitConf($output);
            return $return;
        }
        
        public function splitConf($arr)
        {
            $mainarr=array();
            $subarr=array();
            $count=sizeof($arr);
            $i=0;
            while($i<$count+1) {
                if(isset($arr[$i])) {
                    if(!isset($arr[$i+1])) {
                        $subarr[]=$arr[$i];
                        $i++;
                        $mainarr[]=$subarr;
                    }
                    elseif($arr[$i]->name==$arr[$i+1]->name) {
                        $subarr[]=$arr[$i];
                        $i++;
                        if($arr[$i]==null) {
                            break;
                        }
                    }
                    elseif($arr[$i]->name!==$arr[$i+1]->name) {
                        $subarr[]=$arr[$i];
                        $mainarr[]=$subarr;
                        $subarr=array();
                        $i++;
                        if($arr[$i]==null) {
                            break;
                        }
                    }
                    else {
                        $i++;
                    }
                }
                else {
                    break;
                }
            }
            return $mainarr;
        }
        
        public function getConfValue($id)
        {
            $query="SELECT * from customitems where id=$id";
            $result=mysql_query($query);
            $row=mysql_fetch_object($result);
            $config=new Configurables($row->id, $row->name, $row->value);
            return $config;
        }
        
        public function checkout()
        {
            if(isset($_SESSION['cart'])) {
            $cart = unserialize($_SESSION['cart']);
            //}
            $today = date("Y-m-d H:i:s"); 
            $conf='';
            $query="INSERT INTO purchase (time) values ('$today')";
            $s=mysql_query($query)or die(mysql_error());
            $lastId=mysql_insert_id();
            foreach($cart as $item) {
                if(!empty($item->arr)) {
                    foreach ($item->arr as $val) {
                        $arr=$this->getConfValue($val);
                        $conf .=$arr->name.'-'.$arr->value.' ';
                    }
                }
                $query="INSERT INTO purchaseditems (purchase_id, item_id, count, config) 
                        VALUES ('$lastId', '$item->id', '$item->quantity', '$conf')";
                $sql=mysql_query($query) or die(mysql_error());
                $conf='';
            }
            if($sql==true) {
                unset($_SESSION['cart']);
            }
            return $sql;
            }
            else return false;
        }
    }