<?php
    /*include_once ('application/models/Database.php');
    include_once ('application/models/MySql.php');
    include_once ('application/models/Product.php');
    include_once ('application/models/Category.php');
    include_once ('application/models/Cart.php');
    include_once ('application/models/Type.php');*/
    
    class AdminMod
    {
    
        public function escape($string)
        {
            $escaped=mysql_real_escape_string($string);
            return $escaped;
        }
    
        public function getOutput($query)
        {
            $db=new MySql($query);
            $res=$db->query($query);
            $result=$db->getResult();
            $output=$db->expandResults($result);
            return $output;
        }
        
        public function tirinosaurusRexus($query)
        {
            $db=new MySql($query);
            $res=$db->query($query);
            $result=$db->getResult();
        }
        
        public function getProducts()
        {
            $query="SELECT * FROM product order by id ASC";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function getProduct($id)
        {
            $query="SELECT * FROM product where id=$id";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function getType()
        {
            $query="SELECT * FROM type ORDER BY id ASC";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function categorySelect()
        {
            $query = "SELECT * FROM category ORDER BY parent, name";
            $output=$this->getOutput($query);
            /*$resarr=array();
            $arr=array(
                'items'=>array(),
                'parents'=>array()
            );
            while ($row = mysql_fetch_assoc($result)) { 
                $arr['items'][$row['id']] = $row; 
                $arr['parents'][$row['parent']][] = $row['id']; 
            } 
            $format = $this->buildMenu(0, $arr);*/
            /*
            * Pamatīgs caur žirafes kaklu variants, jāizdomā kaut kas jaukāks ^_^
            */
            $format=$this->trata(0, $output);
            /*$output=new Category($format);
            $resarr[]=$output;
            return $resarr;*/
            return $format;
        }
        
        public function getCategory()
        {
            $query = "SELECT * FROM category ORDER BY parent, name";
            $result = mysql_query($query);
            $resarr=array();
            $arr=array(
                'items'=>array(),
                'parents'=>array()
            );
            while ($row = mysql_fetch_assoc($result)) { 
                $arr['items'][$row['id']] = $row; 
                $arr['parents'][$row['parent']][] = $row['id']; 
            } 
            $format = $this->buildMenu(0, $arr);
            /*
            * Pamatīgs caur žirafes kaklu variants, jāizdomā kaut kas jaukāks ^_^
            */
            $output=new Category($format);
            $resarr[]=$output;
            return $resarr;
        }
        
        public function trata($id, $arr)
        {
            $arre=array(); 
            $subarr=array();
            if (isset($arr->id)) {
                //$html = '<optgroup>';
                foreach ($arr as $item) 
                { 
                    if ($item->parent==0) {
                        /*$html .= '<option value="'.$arr['items'][$itemId]['id'].'">' . $arr['items'][$itemId]['name'].'</option>'; 
                        $html .= '<optgroup></optgroup>';*/
                        $arre[]=$item;
                        
                    }
                    else {
                        $subarr[]=$item;
                    }
                    //$html .= '<option value="'.$arr['items'][$itemId]['id'].'">' . $arr['items'][$itemId]['name'].'</option>'; 
                    //$html .= $this->buildMenu($itemId, $arr); 
                    //$html .= '</option>'; 
                } 
                //$html .= '<optgroup></optgroup>'; 
                
            } 
            return $arre;
        }
        
        function buildMenu($id, $arr)
        {
            $html = ''; 
            if (isset($arr['parents'][$id])) { 
                //$html = '<optgroup>';
                foreach ($arr['parents'][$id] as $itemId) 
                { 
                    if ($arr['items'][$itemId]['parent']==0) {
                        $html .= '<option value="'.$arr['items'][$itemId]['id'].'">' . $arr['items'][$itemId]['name'].'</option>'; 
                        $html .= '<optgroup></optgroup>';
                    }
                    $html .= '<option value="'.$arr['items'][$itemId]['id'].'">' . $arr['items'][$itemId]['name'].'</option>'; 
                    $html .= $this->buildMenu($itemId, $arr); 
                    //$html .= '</option>'; 
                } 
                $html .= '<optgroup></optgroup>'; 
                
            } 
            return $html; 
        } 
        
        
        
        public function getCats()
        {
            $query="SELECT * FROM category ORDER BY id ASC";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function escapePosts()
        {
            $type=$this->escape($_POST['type']);
            $category=$this->escape($_POST['category']);
            $name=$this->escape($_POST['name']);
            $manufacture=$this->escape($_POST['manufacture']);
            $info=$this->escape($_POST['info']);
            $price=$this->escape($_POST['price']);
            $obj=new stdClass();
            $obj->type=$type;
            $obj->category=$category;
            $obj->name=$name;
            $obj->manufacture=$manufacture;
            $obj->info=$info;
            $obj->price=$price;
            return $obj;
        }
        
        public function makePrettyImages()
        {
            $images_location="public/images/";
                $thumbs_location="public/images/thumbs/";
                $thumb_width=100;
                $results="";
                $filename=stripslashes($_FILES['picture']['name']);
                
                $extension=$this->get_image_extension($filename);
                
                $image_random_name=$this->random_name(15).".".$extension;
                                
                $copy=@copy($_FILES['picture']['tmp_name'],$images_location.$image_random_name);
                if(!$copy) {
                    $results="kļūda";
                }
                else {
                    $this->createThumbnails($images_location.$image_random_name,$thumbs_location.$image_random_name, $thumb_width);
                }
                return  $image_random_name;
        }
        
        public function updateProduct($id)
        {
            $variables=$this->escapePosts();

            if(!empty($_FILES['picture']['name'])) {
                $image_random_name=$this->makePrettyImages();
                
                $query="UPDATE product set type='$variables->type', category='$variables->category', name='$variables->name', manufacture='$variables->manufacture',
                                info='$variables->info', price='$variables->price', picture='$image_random_name' where id='$id'";
                $db=new MySql($query);
                $res=$db->query($query);
                $result=$db->getResult();
                return $result;
                
            }
            
            else {
                $query="UPDATE product set type='$variables->type', category='$variables->category', name='$variables->name', manufacture='$variables->manufacture',
                                info='$variables->info', price='$variables->price' where id='$id'";
                $db=new MySql($query);
                $res=$db->query($query);
                $result=$db->getResult();
                return $result;
                
            }
        }
        
        public function createNewProduct()
        {
        
        }
        
        public function addNewProduct()
        {
            var_dump($_POST);
            var_dump($_GET);
            var_dump($_FILES);
            $variables=$this->escapePosts();
            
            $query="SELECT * FROM product where name='$variables->name'";
            $output=$this->getOutput($query);
            if ($output!=null) {
                echo '<div class="no">Šāds ieraksts jau eksistē!</div>';
                return false;
            }
            
            if(!empty($_FILES['picture']['name'])) {
                $image_random_name=$this->makePrettyImages();
                
                $query="INSERT INTO product (type, category, name, manufacture, info, price, picture) 
                    VALUES ('$variables->type', '$variables->category', '$variables->name', '$variables->manufacture', '$variables->info', '$variables->price', '$image_random_name')";
                $db=new MySql($query);
                $res=$db->query($query);
                $result=$db->getResult();
                return $result;
            }

            else {
                $query="INSERT INTO product (type, category, name, manufacture, info, price) 
                    VALUES ('$variables->type', '$variables->category', '$variables->name', '$variables->manufacture', '$variables->info', '$variables->price')";
                $db=new MySql($query);
                $res=$db->query($query);
                $result=$db->getResult();
                return $result;
            }
        }
        
        public function deleteProduct($id)
        {
            $query="DELETE from product WHERE id=$id";
            $db=new MySql($query);
            $res=$db->query($query);
            $result=$db->getResult();
            return $result;
        }
        
        public function createThumbnails($source, $destination, $thumb_width)
        {
            $size=getimagesize($source);
            $width=$size[0];
            $height=$size[1];
            $x=0;
            $y=0;
            $image="";
            
            if($width>$height) {
                $x=ceil(($width-$height)/2);
                $width=$height;
            }
            elseif($height>$width) {
                $y=ceil(($height-$width)/2);
                $height=$width;
            }
            
            $new_image=imagecreatetruecolor($thumb_width, $thumb_width) or die('Nevar izveidot jaunu attēlu');
            
            $extension=$this->get_image_extension($source);
            
            if($extension=='jpg'||$extension=='jpeg') {
                $image=imagecreatefromjpeg($source);
            }
            if($extension=='gif') {
                $image=imagecreatefromgif($source);
            }
            if($extension=='png') {
                $image=imagecreatefrompng($source);
            }
            
            imagecopyresampled($new_image,$image,0,0,$x,$y,$thumb_width,$thumb_width,$width, $height);
            
            if($extension=='jpg'||$extension=='jpeg') {
                imagejpeg($new_image,$destination);
            }
            if($extension=='gif') {
                imagegif($new_image, $destination);
            }
            if($extension=='png') {
                imagepng($new_image, $destination);
            }
        }
        
        public function get_image_extension($name)
        {
            $name=strtolower($name);
            $i=strpos($name,".");
            if (!$i) {
                return "";
            }
            $l=strlen($name)-$i;
            $extension=substr($name, $i+1, $l);
            return $extension;
        }
        
        public function random_name($length)
        {
            $characters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            $name="";
            for($i=0; $i<$length; $i++) {
                $name .=$characters[mt_rand(0,strlen($characters)-1)];
            }
            return "image-".$name;
        }
        
        public function displayPurchases()
        {
            $query="SELECT * FROM purchase ORDER BY time DESC";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function displayPurchaseDet($id)
        {
            $query="SELECT product.id, product.name, purchasedItems.count, purchasedItems.config FROM purchasedItems 
                INNER JOIN product on purchasedItems.item_id=product.id  where purchasedItems.purchase_id=$id ORDER BY product.name";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function addCustomisables()
        {
            $query="SELECT * FROM customitems ORDER BY name";
            $output=$this->getOutput($query);
            $return=$this->splitConf($output);
            return $return;
        }
                
        public function splitConf($arr)
        {
            $mainarr=array();
            $subarr=array();
            $count=sizeof($arr);
            $i=0;
            while($i<$count+1) {
                if(isset($arr[$i])) {
                    if(!isset($arr[$i+1])) {
                        $subarr[]=$arr[$i];
                        $i++;
                        $mainarr[]=$subarr;
                    }
                    elseif($arr[$i]->name==$arr[$i+1]->name) {
                        $subarr[]=$arr[$i];
                        $i++;
                        if($arr[$i]==null) {
                            break;
                        }
                    }
                    elseif($arr[$i]->name!==$arr[$i+1]->name) {
                        $subarr[]=$arr[$i];
                        $mainarr[]=$subarr;
                        $subarr=array();
                        $i++;
                        if($arr[$i]==null) {
                            break;
                        }
                    }
                    else {
                        $i++;
                    }
                }
                else {
                    break;
                }
            }
            return $mainarr;
        }
        
        public function getCustom($id)
        {
            $query="SELECT * FROM custom where prodId=$id order by customId";
            $output=$this->getOutput($query);
            return $output;
        }
        
        public function createCustom($arr)
        {
        
        }
        
        public function updateCustom($id)
        {
        
        }
        
        public function deleteCustom($id)
        {
        
        }
        
        public function createCategory($arr)
        {
            
        }
        
        public function updateCategory($id)
        {

        }
        
/*Nepārbaudīts! Es varu strādāt ar 3 kverijiem vienā metodē? Vajag pārbaudi, vai ieraksts ir db un, vai kategorijai ir subkati*/        
        public function deleteCategory($id)
        {
            $parent="";
            $query="SELECT * FROM category WHERE id=$id";
            $parentres=$this->getOutput($query);
            if(mysql_num_rows($parentres)>0) {
                if($parentres->id=$id) {
                    if($parentres->parent==0) {
                        $parent=0;
                    }
                    else {
                        $parent=$parentres->parent;
                    }
                }
            }
            $query="UPDATE category SET parent=$parent WHERE parent=$id";
            $update=$this->tirinosaurusRexus($query);
            $query="DELETE FROM category WHERE id=$id";
            $del=$this->tirinosaurusRexus($query);
        }
        
        public function updateCustoms($id)
        {
            $prearr=array();
            $postarr=array();
            $postarr=$_POST['property'];
            $default=$this->getCustom($id);
            foreach($default as $prop) {
                $prearr[]=$prop->customId;
            }
            sort($postarr, asort($postarr));
            $deleted = array_diff($prearr, $postarr);
            $added = array_diff($postarr, $prearr);
            if(!empty($deleted)) {
                foreach($deleted as $post){
                    $query="DELETE FROM custom WHERE prodId=$id AND customId=$post";
                    $del=$this->tirinosaurusRexus($query);
                }
            }
            if(!empty($added)) {
                foreach($added as $pre){
                    $query="INSERT INTO custom (prodId, customId) values ($id, $pre)";
                    $add=$this->tirinosaurusRexus($query);
                }
            }
        }
    }