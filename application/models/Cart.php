<?php
    class Cart
    {
        public $id;
        public $name;
        public $info;
        public $price;
        public $quantity;
        public $arr;
        
        public function __construct($id, $name, $info, $price, $picture, $quantity, $arr)
        {
                $this->id=$id;
                $this->name=$name;
                $this->info=$info;
                $this->price=$price;
                $this->picture=$picture;
                $this->quantity=$quantity;
                $this->arr=$arr;

        }
   
    }
    