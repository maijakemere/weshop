<?php
    /*include_once ('Product.php');*/
    class Book extends Product
    {
        public $pagecount;
        public $author;
        public $year;
        public $genre;
        
        public function __construct($id, $type, $category, $name, $manufacture, $info, $price, $picture, $pagecount, $author, $year, $genre)
        {
            parent::__construct($id, $type, $category, $name, $manufacture, $info, $price, $picture);
            $this->pagecount=$pagecount;
            $this->author=$author;
            $this->year=$year;
            $this->genre=$genre;
        }
    }