<?php    
    class Product
    {
        public $id;
        public $type;
        public $category;
        public $name;
        public $manufacture;
        public $info;
        public $price;
        public $picture;
        
        public function __construct($id, $type, $category, $name, $manufacture, $info, $price, $picture)
        {
            $this->id=$id;
            $this->type=$type;
            $this->category=$category;
            $this->name=$name;
            $this->manufacture=$manufacture;
            $this->info=$info;
            $this->price=$price;
            $this->picture=$picture;
        }
   
    }