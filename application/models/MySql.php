<?php 
    //include_once ('application/models/Database.php');
    class MySql extends Database
    {
        public $query;
        public $results;
        
        public function __construct($sql)
        {
            $this->sql=$sql;
        }
        
        public function query($sql)
        {
            $this->results=mysql_query($sql) or die(mysql_error());
        }
        
        public function getResult()
        {
            return $this->results;
        }
        
        public function expandResults($result)
        {
            $arr=array();
            while($row=mysql_fetch_object($result)) {
                $arr[]=$row;
            }
            return $arr;
        }
    }