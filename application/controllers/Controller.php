<?php
/**
 * Class Controller
 */
    class Controller
    {
        public $model;

        /**
         * Constructs new class Model object
         */
        public function __construct()
        {
            $this->model=new Model();
        }

        /**
         * @param $arr array
         * @param $path string
         */
        public function view($arr, $path)
        {
            include 'application/views/'.$path.'.php';
        }

        public function index() {
            $products = $this->model->getProductList(0);
            $pages = $this->model->getPages();
            $arr = array ($products, $pages);
            $this->view($arr, 'Productlist');
        }

        /**
         * @param $page float
         */
        public function p($page)
        {
             $products=$this->model->getProductList($page);
             $pages=$this->model->getPages($page);
             $arr = array ($products, $pages);
             $this->view($arr, 'Productlist');
        }

        /**
         * @param $id string (in DB represented as integer)
         */
        public function category($id)
        {
            $products=$this->model->getCatProducts($id);
            $pages=$this->model->getCatPages($id);
            $arr = array ($products, $pages);
            $this->view($arr, 'Productlist');
        }
        
        public function categories()
        {
            $categories=$this->model->showCategories();
            $arr = array($categories);
            $this->view($arr, 'Categories');
        }

        /**
         * @param $id string (in DB represented as integer)
         */
        public function item($id)
        {
            $items=$this->model->createCart($id);
            $arr = array($items);
            $this->view($arr, 'CartItems');
        }

        /**
         * @param $id string (in DB represented as integer)
         */
        public function remove($id)
        {
            $items=$this->model->removeFromCart($id);
            $arr = array($items);
            $this->view($arr, 'CartItems');
        }
        
        public function cart()
        {
            $items=$this->model->returnCart();
            $arr = array($items);
            $this->view($arr, 'Cartitems');
        }

        /**
         * @param $id string (in DB represented as integer)
         */
        public function config($id)
        {
            /*Pilnībā izņem produktu no groza un aizsūta uz produkta lapu.
            *Ja produkts netiek konfigurēts, grozā ievieto pamata produktu bez konfigurācijām.
            *Man šis risinājums ļoti, ļoti nepatīk. :( */
            $config=$this->model->removeFromCart($id);
            $this->item($id);
            //$items=$this->model->createCart($id);
        }

        /**
         * @param $id string (in DB represented as integer)
         */
        public function id($id)
        {
            $product=$this->model->getProduct($id);
            $customs=$this->model->getCustom($id);
            $arr = array($product, $customs);
            $this->view($arr, 'Product');
        }
        
        public function checkout()
        {
            $checkouts=$this->model->checkout();
            $arr = array($checkouts);
            $this->view($arr, 'Checkout');
        }
    }
