<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Maija
 * Date: 13.14.3
 * Time: 13:47
 * To change this template use File | Settings | File Templates.
 */
    class Core{
        function run()
        {
            if (isset($_SERVER['PATH_INFO'])) {
                $param = $_SERVER['PATH_INFO'];
                $path = explode('/', $param);
                for ($i = 2; $i < sizeof($path); $i += 2)
                {
                    if (isset($path[$i+1])) {
                        $pather = new Controller();
                        $pather->$path[$i]($path[$i+1]);
                        break;
                    }  else {
                        echo $i;
                        $pather = new Controller();
                        $pather->$path[$i]();
                        break;
                    }
                }
            } else {
                $path = 'index';
                $pather = new Controller();
                $pather->$path();
            }
        }
    }