<?php
/**
 * Class Admin
 */
    class Admin
    {
    
        public $mod;

        /**
         * Default constructor for new AdminMod class object
         */
        public function __construct()
        {
            $this->mod=new AdminMod();
        }

        /**
         * @param $arr array
         * @param $path string
         */
        public function view($arr, $path)
        {
            include 'application/views/Admin/'.$path.'.php';
        }

        public function showProducts()
        {
            $products=$this->mod->getProducts();
            $arr = array($products);
            $this->view($arr, 'AdminProductList');
        }
        
        public function showProduct($id)
        {
            $product=$this->mod->getProduct($id);
            $categories=$this->mod->getCategory();
            $cats=$this->mod->getCats();
            $types=$this->mod->getType();
            $customs=$this->mod->addCustomisables();
            $allcustoms=$this->mod->getCustom($id);
            $arr = array($product, $categories, $cats, $types, $customs, $allcustoms);
            $this->view($arr, 'AdminProduct');
        }
        
        public function editProduct($id)
        {
            $product=$this->mod->updateProduct($id);
            $types=$this->mod->getType();
            $categories=$this->mod->getCategory();
            $cats=$this->mod->getCats();
            $customs=$this->mod->getCustom($id);
            $customs=$this->mod->updateCustoms($id);
            $this->showProducts();
        }
        
        public function createProduct()
        {
            $product=$this->mod->createNewProduct();
            $categories=$this->mod->getCategory();
            $types=$this->mod->getType();
            $customs=$this->mod->addCustomisables();
            include 'application/views/Admin/AddNewProduct.php';
        }
        
        public function addProduct()
        {
            $product=$this->mod->addNewProduct();
            $this->showProducts();
        }
        
        public function deleteProduct($id)
        {
            $product=$this->mod->deleteProduct($id);
            $this->showProducts();
        }
        
        public function viewPurchases()
        {
            $purchases=$this->mod->displayPurchases();
            include 'application/views/Admin/DisplayPurchases.php';
        }
        
        public function viewPurchasDetails($id)
        {
            $purchases=$this->mod->displayPurchaseDet($id);
            include 'application/views/Admin/DisplayPurchaseDet.php';
        }
        
        public function viewCategories()
        {
            $category=$this->mod->viewCategories();
            include 'application/views/Admin/Categories.php';
        }
    }